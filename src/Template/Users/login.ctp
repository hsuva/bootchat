<?php  $this->assign('title', __('Login')); ?>

<?php 
    $this->Form->setTemplates([
        'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
    ])
?>
<div class="users login">
    <div class="row justify-content-md-center">
        <div class="col col-md-4">
            <div class="card">
                <div class="card-body">
                    <?= $this->Form->create(null, ['novalidate' => true]) ?>
                    <?= $this->Form->control('username', ['class' => 'form-control']) ?>
                    <?= $this->Form->control('password', ['class' => 'form-control']) ?>
                    <div class="form-group">
                        <?= $this->Form->button('Login', ['class' => 'btn btn-dark btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>