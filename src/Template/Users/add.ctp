<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="users form">
    <div class="row justify-content-md-center">
        <div class="col col-md-4">
            <div class="card">
                <div class="card-body">
                    <?php 
                        $this->Form->setTemplates([
                            'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
                            'inputContainerError' => '<div class="form-group {{type}}{{required}} error">{{content}}{{error}}</div>',
                        ])
                    ?>
                    <?= $this->Form->create($user, ['novalidate']) ?>    
                        <?php
                            echo $this->Form->control('username', [
                                'class' => 'form-control'                
                            ]);
                            echo $this->Form->control('password', [
                                'class' => 'form-control'                
                            ]);
                            echo $this->Form->control('repeat_password', [
                                'class' => 'form-control',
                                'type' => 'password'
                            ]);
                        ?>
                    <div class="form-group">
                        <?= $this->Form->button(__('Register'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
