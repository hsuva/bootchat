<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages *
 
 */
class MessagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {        
        $this->paginate = [
            'contain' => ['Users'],
            'order' => ['id' => 'DESC'],
            'limit' => 10
        ];
        $messages = $this->paginate($this->Messages);

        $collection = new Collection($messages);
        $messages = $collection->sortBy('id', SORT_ASC);

        $this->set(compact('messages'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response Return json data
     */
    public function add()
    {
        $success = false;
        $message = $this->Messages->newEntity();
        if ($this->request->is('post')) {            
            $message = $this->Messages->patchEntity($message, $this->request->getData());            
            $message->user_id = $this->Auth->user('id');
            $message->content = $this->request->data['message']['text'];
            
            if ($this->Messages->save($message)) {
                $success = true;                
            }
        }

        $result = json_encode(['success' => $success]);

        $this->response->type('json');
        $this->response->body($result);
        return $this->response;        
    }
}