## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Download Git (https://git-scm.com/downloads)

If Git and Composer are installed globally, run

```bash
git clone https://hsuva@bitbucket.org/hsuva/bootchat.git
cd bootchat
composer install
```

Using MySQL connection, create your local schema and edit `config/app.php` and setup the `'Datasources'` configuration.

# Run all the migrations
```bash
bin/cake migrations migrate
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Since this skeleton is a starting point for your application and various files
would have been modified as per your needs, there isn't a way to provide
automated upgrades, so you have to do any updates manually.

## Layout

The app skeleton uses a subset of [Foundation](http://foundation.zurb.com/) (v5) CSS
framework by default. You can, however, replace it with any other library or
custom styles.
