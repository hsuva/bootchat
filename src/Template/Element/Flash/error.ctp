<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger mb-20" onclick="this.classList.add('hidden');"><?= $message ?></div>
