<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Message[]|\Cake\Collection\CollectionInterface $messages
 */
?>
<div class="messages index">
    <div class="row justify-content-md-center">
        <div class="col col-md-6">
            <div class="card">
                <div class="card-header">                    
                    <span id="activeUsers" class="badge badge-success badge-pill">0</span> <small class="text-muted">active users</small>
                </div>
                <div class="card-body">
                    <div id="main-container">                            
                        <ul id="messageLists" class="list-unstyled">
                            <?php if($messages): ?>                            
                                <?php foreach($messages as $message): ?>
                                <li class="media my-2">
                                    <img class="mr-3" src="http://placehold.it/50&text=<?= strtoupper(substr($message->user->username, 0, 1)) ?>" width="40">
                                    <div class="media-body">
                                    <h5 class="mt-0 mb-1">
                                        <small class="float-right text-muted"><?= $message->created->format('h:m A') ?></small>
                                        <?= $message->user->username ?>
                                    </h5>
                                    <?= $message->content ?>
                                    </div>
                                </li>
                                <?php endforeach ?>
                            <?php endif ?>
                        </ul>
                        <div class="input-group" id="msg-container">
                            <input type="text" class="form-control" id="txtMessage" name="msg">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" disabled="disabled" id="btnSend">Send</button>
                            </div>
                        </div>
                    </div>                    
                </div>                
            </div>
        </div>
    </div>
</div>

<script id="messageTemplate" type="text/x-handlebars-template">    
    <li class="media my-2">
        <img class="mr-3" src="http://placehold.it/50&text={{initial}}" width="40">
        <div class="media-body">
        <h5 class="mt-0 mb-1">
            <small class="float-right text-muted">{{time}}</small>
            {{user}}
        </h5>
        {{text}}                
        </div>
    </li>    
</script>
<?php //echo $this->Html->script('main.js', ['block' => 'scriptPlugin']) ?>

<script>
<?php $this->Html->scriptStart(['block' => 'scriptBottom']); ?>
(function(){   

    var username = '<?= $this->request->session()->read('Auth.User.username')?>';
    
    var tmpl = Handlebars.compile($('#messageTemplate').html());
    
    function updateMessages(msg){                
        var messageList = tmpl(msg);
        $('#messageLists').append(messageList);
        $('#btnSend').attr('disabled', 'disabled');
        scrollMessagesToBottom();
    }

    function scrollMessagesToBottom(){
        $("#messageLists").animate({ scrollTop: $('#messageLists')[0].scrollHeight}, 1000);
    }

    var conn = new WebSocket('ws://localhost:8080');

    conn.onopen = function(e) {
        console.log("conn.onopen!");        
    };

    conn.onclose = function(e) {
        console.log("conn.onclose!");
        $.ajax({
            headers: {
                'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')) ?>
            },
            url : '/users/logout',
            type: 'GET',
            success: function(){
                window.location.href = '/users/login';
            },             
            error: function(){
                alert('There was an error while sending  your request. Please contact administrator.');
            }
        });
    }

    conn.onmessage = function(e) {
        console.log("conn.onmessage!");
        
        var r = JSON.parse(e.data);        
        
        if(r.hasOwnProperty('event')){
            switch(r.event){
                case 'open':
                    $('#activeUsers').text(r.data);
                    break;
                case 'close':
                    $('#activeUsers').text(r.data);
                    break;
                case 'message':
                    updateMessages(r);
                    break;
                default:
                    break;

            }
        }        
    };

    $('#btnSend').click(function(){
        var text = $('#txtMessage').val();

        if(text.match(/^(?:\/quit)/)){
            conn.close()
        }else{
            var msg = {
                'user': username,
                'initial': username.charAt(0).toUpperCase(),
                'text': text,
                'time': moment().format('hh:mm a'),
                'event': 'message'
            };
            $.ajax({
                headers: {
                    'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')) ?>
                },
                url : '/messages/add',
                type: 'POST',
                dataType: 'json',
                data: { message : msg },
                success: function(response){                
                    updateMessages(msg);
                    conn.send(JSON.stringify(msg));
                },
                error: function(){
                    alert('There was an error end your message. Please contact administrator.');
                }
            });
        }
        $('#txtMessage').val('');
    });


    $('#txtMessage').on('input', function(){
        if($(this).val().length === 0){        
            $('#btnSend').attr('disabled', 'disabled');
        }else{
            $('#btnSend').removeAttr('disabled');
        }
    }).on('keyup', function(e){
        if(e.keyCode == 13){
            $('#btnSend').trigger('click');
        }
    })

    scrollMessagesToBottom();

    setTimeout(function(){ 
        conn.close(); 
    }, 60000);
})();
<?php $this->Html->scriptEnd(); ?>
</script>